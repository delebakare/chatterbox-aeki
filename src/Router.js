import React from "react";
import { Route, Switch } from "react-router-dom";

/** Components **/
// Home
import Dashboard from './views/Dashboard/Dashboard'
// Student
import Student from "./views/Student/Student";
// 404
import NotFound from "./views/NotFound/NotFound";


const Router = props => (
    <Switch>
        {/* Home */}
        <Route exact path="/" component={Dashboard} />
        {/* Student */}
        <Route path="/student/:student" component={Student} />

        {/*Page Not Found*/}
        <Route component={NotFound} />
    </Switch>
);

export default Router;
