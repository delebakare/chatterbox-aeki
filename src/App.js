import React from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import Router from './Router';

function App() {
  return (
    <BrowserRouter>
      <Route component={Router} />
    </BrowserRouter>
  );
}

export default App;