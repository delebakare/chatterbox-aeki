import React from 'react';
import { Link } from "react-router-dom";

function Sidebar() {
    return (
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/">
                <div className="sidebar-brand-icon rotate-n-15">
                    <i className="fas fa-tachometer-alt"></i>
                </div>
                <div className="sidebar-brand-text mx-3">AEKI</div>
            </a>

            <hr className="sidebar-divider my-0" />

            <li className="nav-item active">
                <Link className="nav-link" to="/">
                    <i className="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></Link>
            </li>

            {/* <hr className="sidebar-divider my-0" />

            <li className="nav-item">
                <a className="nav-link" href="/">
                    <i className="fas fa-fw fa-user-alt"></i>
                    <span>View Students</span></a>
            </li> */}

            <hr className="sidebar-divider d-none d-md-block" />

            <div className="text-center d-none d-md-inline">
                <button className="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
    );
}

export default Sidebar;