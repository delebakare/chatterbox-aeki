import React from 'react';

import Loader from 'react-loader-spinner'

function Content(props) {
    let data = props.data;

    return (

        props.loading ? < div className="d-flex justify-content-center" style={{ paddingTop: '250px' }}><Loader type="Oval" color="#4E73DF" /></div> :
            <div className="row">

                <div className="col-lg-12">
                    <div className="card mb-4">
                        <div className="card-header">
                            {data.studentsName}'s Profile
                    </div>
                        <div className="card-body">
                            <h6><strong>Organisation:</strong> {data.studentsOrganisation}</h6>
                            <h6><strong>Full Name:</strong> {data.studentsName}</h6>
                            <h6><strong>Email:</strong> {data.studentsEmail}</h6>
                            <h6><strong>Total Credits:</strong> {data.studentsTotalCredits}</h6>
                            <h6><strong>Remaining Credits:</strong> {data.studentsRemainingCredits}</h6>
                            <h6><strong>Status:</strong> <span className={`badge ${data.studentsCertified ? 'badge-success' : 'badge-danger'}`} >{data.studentsCertified ? 'Certified' : 'Not Certified'}</span></h6>
                            <h6><strong>Date of Enrollment:</strong> {data.studentEnrolled}</h6>
                            <h6><strong>Last Booking Date:</strong> {data.studentLastBookingDate}</h6>
                        </div>
                    </div>
                </div>
            </div>
    );
}

export default Content;