import React, { Component } from 'react';
import Content from './Content';

import Sidebar from '../../components/Sidebar/Sidebar';
import Nav from "../../components/Nav/Nav";

import axios from 'axios';
import Loader from 'react-loader-spinner'

class Student extends Component {
    constructor() {
        super();
        this.state = {
            studentId: '',
            data: [],
            studentLoading: false
        }

        this.getStudentData = this.getStudentData.bind(this);
        this.loadScripts = this.loadScripts.bind(this);
    }

    loadScripts() {
        //Load scripts
        // "ACTIVE (ON TRACK)", "ACTIVE (SLOW)", "INACTIVE"
        // studentsOnTrackCount studentsSlowCount studentsInactiveCount

        let scripts = [
            { src: "../vendor/jquery/jquery.min.js" },
            { src: "../vendor/bootstrap/js/bootstrap.bundle.min.js" },
            { src: "../vendor/jquery-easing/jquery.easing.min.js" },
            { src: "../js/sb-admin-2.min.js" },
            { src: "../vendor/chart.js/Chart.min.js" },
            { src: "../js/demo/chart-area-demo.js" },
            { src: "../js/demo/chart-pie-demo.js" },

        ];
        scripts.map(item => {
            let script = document.createElement("script");
            script.src = item.src;
            script.async = true;
            document.body.appendChild(script);
        });
    }

    getStudentData(id) {
        // Fetch student data from API endpoint
        this.setState({ studentLoading: true });
        axios.get(`http://3.84.19.167:8000/api/studentway/${id}/`)
            .then(json => {
                // console.log(json.data.student.organisation);
                if (json.data.status === 200) {
                    let studentData = {
                        studentId: json.data.student.id,
                        studentsOrganisation: json.data.student.organisation,
                        studentsName: json.data.student.name,
                        studentsEmail: json.data.student.email,
                        studentsTotalCredits: json.data.student.total_credits,
                        studentsRemainingCredits: json.data.student.remaining_credits,
                        studentsCertified: json.data.student.is_certified,
                        studentEnrolled: json.data.student.enrolled_date,
                        studentLastBookingDate: json.data.student.last_booking_date,
                    };
                    this.setState({
                        data: studentData,
                        studentLoading: false
                    });
                } else {
                    alert(`Our System Failed To Load Student Data!`);
                }
            }).catch(error => {
                if (error.response) {
                    // The request was made and the server responded with a status code that falls out of the range of 2xx
                    let err = error.response.data;
                    this.setState({
                        error: err.message,
                        errorMessage: err.errors,
                        studentLoading: false
                    })
                }
                else if (error.request) {
                    // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                    let err = error.request;
                    this.setState({
                        error: err,
                        studentLoading: false
                    })
                } else {
                    // Something happened in setting up the request that triggered an Error
                    let err = error.message;
                    this.setState({
                        error: err,
                        dashboardLoading: false
                    })
                }
            }).finally(
                this.setState({ error: '' })
            );
    }

    componentDidMount() {
        this.loadScripts();

        const { student } = this.props.match.params;

        this.getStudentData(student);

        console.log(this.state.data);
    }

    componentWillUnmount() {
        let script = document.getElementsByTagName("script");
        let i = script.length;
        while (i--) {
            script[i].parentNode.removeChild(script[i]);
        }
    }

    render() {
        let studentData = this.state.data;
        let loading = this.state.studentLoading;
        // console.log(studentData)
        return (
            <div id="wrapper">
                <Sidebar />

                <div id="content-wrapper" className="d-flex flex-column">
                    <div id="content">
                        <Nav />
                        <Content data={studentData} loader={Loader} loading={loading} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Student;