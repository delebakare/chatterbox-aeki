import React, { Component } from "react";

import Sidebar from '../../components/Sidebar/Sidebar';
import Nav from '../../components/Nav/Nav';
import Content from './Content';

import axios from 'axios';

class Dashboard extends Component {
    constructor() {
        super();

        this.state = {
            data: [],
            error: '',
            errorMessage: '',
            dashboardLoading: false
        };

        this.getDashboardData = this.getDashboardData.bind(this);
        this.loadScripts = this.loadScripts.bind(this);
        this.loadTableScripts = this.loadTableScripts.bind(this);
        this.loadChartScripts = this.loadChartScripts.bind(this);
    }

    getDashboardData() {
        this.setState({ dashboardLoading: true });
        // Fetch student data from API endpoint
        axios.get("http://3.84.19.167:8000/api/dashboard/")
            .then(json => {
                // console.log(json.data);
                if (json.data.status === 200) {
                    let dashboardData = {
                        student: json.data.student,
                        studentsLoading: false,
                        studentsCount: json.data.total_no_students,
                        studentsCertifiedCount: json.data.no_certified_students,
                        studentsInactiveCount: json.data.inactive,
                        studentsSlowCount: json.data.slow,
                        studentsOnTrackCount: json.data.on_track,
                        coursesCount: json.data.total_no_courses,
                    };
                    this.setState({
                        data: dashboardData,
                        dashboardLoading: false
                    });
                } else {
                    alert(`Our System Failed To Load Dashboard Data!`);
                }
            }).catch(error => {
                if (error.response) {
                    // The request was made and the server responded with a status code that falls out of the range of 2xx
                    let err = error.response.data;
                    this.setState({
                        error: err.message,
                        errorMessage: err.errors,
                        dashboardLoading: false
                    })
                }
                else if (error.request) {
                    // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
                    let err = error.request;
                    this.setState({
                        error: err,
                        dashboardLoading: false
                    })
                } else {
                    // Something happened in setting up the request that triggered an Error
                    let err = error.message;
                    this.setState({
                        error: err,
                        dashboardLoading: false
                    })
                }
            }).finally(
                this.setState({ error: '' })
            );
    }

    async loadScripts() {
        //Load scripts
        // "ACTIVE (ON TRACK)", "ACTIVE (SLOW)", "INACTIVE"
        // studentsOnTrackCount studentsSlowCount studentsInactiveCount

        let scripts = [
            { src: "vendor/jquery/jquery.min.js" },
            { src: "vendor/bootstrap/js/bootstrap.bundle.min.js" },
            { src: "vendor/jquery-easing/jquery.easing.min.js" },
            { src: "js/sb-admin-2.min.js" },
            { src: "vendor/chart.js/Chart.min.js" },
            { src: "js/demo/chart-area-demo.js" },
            { src: "js/demo/chart-pie-demo.js" },

        ];
        scripts.map(item => {
            let script = document.createElement("script");
            script.src = item.src;
            script.async = true;
            document.body.appendChild(script);
        });
    }

    loadChartScripts() {
        let scripts = [
            { src: "vendor/chart.js/Chart.min.js" },
            { src: "js/demo/chart-area-demo.js" },
            { src: "js/demo/chart-pie-demo.js" },

        ];
        scripts.map(item => {
            let script = document.createElement("script");
            script.src = item.src;
            script.async = true;
            document.body.appendChild(script);
        });
    }

    async loadTableScripts() {
        let scripts = [
            { src: "vendor/datatables/jquery.dataTables.min.js" },
            { src: "vendor/datatables/dataTables.bootstrap4.min.js" },
            { src: "js/demo/datatables-demo.js" }
        ];
        scripts.map(item => {
            let script = document.createElement("script");
            script.src = item.src;
            script.async = true;
            document.body.appendChild(script);
        });
    }

    componentDidMount() {
        this.getDashboardData();
        // this.timer = setInterval(() => this.getDashboardData(), 5000);
        this.loadScripts();
        this.loadChartScripts();
        // this.loadTableScripts();
        // Load Scripts for slow networks
        window.setTimeout(() => this.loadScripts(), 4000);
        window.setTimeout(() => this.loadChartScripts(), 4000);
        // window.setTimeout(() => this.loadTableScripts(), 500);
        // this.timer = setInterval(() => this.loadChartScripts(), 10000);
    }

    componentWillUnmount() {
        let script = document.getElementsByTagName("script");
        let i = script.length;
        while (i--) {
            script[i].parentNode.removeChild(script[i]);
        }
    }

    render() {
        // console.log(this.state.data.students)
        let dashboardData = this.state.data;
        // console.log(dashboardData)
        let loading = this.state.dashboardLoading;
        return (
            <div id="wrapper">
                <Sidebar />

                <div id="content-wrapper" className="d-flex flex-column">
                    <div id="content">
                        <Nav />

                        <Content data={dashboardData} loading={loading} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;