import React from 'react';
import { Link } from "react-router-dom";

import Loader from 'react-loader-spinner'

function Content(props) {
    // console.log(props.data)
    let data = props.data;

    function renderTable() {
        if (data.student)
            return data.student.map((student, index) => {
                const { id, organisation, name, email, total_credits, remaining_credits, is_certified, status, enrolled_date, last_booking_date } = student;
                return (
                    <tr key={index}>
                        <td>{id}</td>
                        <td>{organisation}</td>
                        <td>{name}</td>
                        <td>{email}</td>
                        <td>{total_credits}</td>
                        <td>{remaining_credits}</td>
                        <td><span className={`badge ${is_certified ? 'badge-success' : 'badge-danger'}`} >{is_certified ? 'Certified' : 'Not Certified'}</span></td>
                        <td><span className={`badge ${status === 'Active- On Track' ? 'badge-success' : status === 'Active- Slow' ? 'badge-warning' : 'badge-danger'}`} >{status === 'Active- On Track' ? 'Active- On Track' : status === 'Active- Slow' ? 'Active- Slow' : 'Inactive'}</span></td>
                        <td>{enrolled_date}</td>
                        <td>{last_booking_date}</td>
                        <td><Link to={`student/${id}`}>View</Link></td>
                    </tr>
                );
            });
    }

    return (
        props.loading ? < div className="d-flex justify-content-center" style={{ paddingTop: '250px' }}><Loader type="Oval" color="#4E73DF" /></div> :
            < div className="container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>

                <div className="row">
                    {/* Students */}
                    <div className="col-xl-4 col-md-6 mb-4">
                        <div className="card border-left-primary shadow h-100 py-2">
                            <div className="card-body">
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">Students</div>
                                        <div className="h5 mb-0 font-weight-bold text-gray-800">{data.studentsCount}</div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fas fa-user-graduate fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Courses */}
                    <div className="col-xl-4 col-md-6 mb-4">
                        <div className="card border-left-info shadow h-100 py-2">
                            <div className="card-body">
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div className="text-xs font-weight-bold text-info text-uppercase mb-1">Courses</div>
                                        <div className="row no-gutters align-items-center">
                                            <div className="col-auto">
                                                <div className="h5 mb-0 mr-3 font-weight-bold text-gray-800">{data.coursesCount}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fas fa-book fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Certified */}
                    <div className="col-xl-4 col-md-6 mb-4">
                        <div className="card border-left-warning shadow h-100 py-2">
                            <div className="card-body">
                                <div className="row no-gutters align-items-center">
                                    <div className="col mr-2">
                                        <div className="text-xs font-weight-bold text-warning text-uppercase mb-1">Certified</div>
                                        <div className="h5 mb-0 font-weight-bold text-gray-800">{data.studentsCertifiedCount}</div>
                                    </div>
                                    <div className="col-auto">
                                        <i className="fas fa-star fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div className="col-xl-3 col-md-6 mb-4">
                    <div className="card border-left-success shadow h-100 py-2">
                        <div className="card-body">
                            <div className="row no-gutters align-items-center">
                                <div className="col mr-2">
                                    <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Progress</div>
                                    <div className="h5 mb-0 font-weight-bold text-gray-800">{data.studentsOnTrackCount}</div>
                                </div>
                                <div className="col-auto">
                                    <i className="fas fa-fast-forward fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> */}
                </div>

                <div className="row">
                    <div className="col-xl-12 col-lg-12">
                        <div className="card shadow mb-4">
                            <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 className="m-0 font-weight-bold text-primary">Student Status</h6>
                            </div>
                            <div className="card-body">
                                <div className="chart-pie pt-4 pb-2">
                                    <canvas id="myPieChart"></canvas>
                                    <div id="studentsOnTrackCount" value={data.studentsOnTrackCount}></div>
                                    <div id="studentsSlowCount" value={data.studentsSlowCount}></div>
                                    <div id="studentsInactiveCount" value={data.studentsInactiveCount}></div>
                                </div>
                                <div className="mt-4 text-center small">
                                    <span className="mr-2">
                                        <i className="fas fa-circle text-primary"></i> ACTIVE (ON TRACK): <strong>{data.studentsOnTrackCount}</strong>
                                    </span>
                                    <span className="mr-2">
                                        <i className="fas fa-circle text-success"></i> ACTIVE (SLOW): <strong>{data.studentsSlowCount}</strong>
                                    </span>
                                    <span className="mr-2">
                                        <i className="fas fa-circle text-info"></i> INACTIVE: <strong>{data.studentsInactiveCount}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                    {/* <div className="col-lg-6">

                    <div className="card position-relative">
                        <div className="card-header py-3">
                            <h6 className="m-0 font-weight-bold text-primary">Progress</h6>
                        </div>
                        <div className="card-body">
                        </div>
                    </div>

                </div> */}

                    <div className="col-lg-12">

                        <div className="card position-relative">
                            <div className="card-header py-3">
                                <h6 className="m-0 font-weight-bold text-primary">All Students</h6>
                            </div>
                            <div className="card-body">
                                <div className="card shadow mb-4">
                                    <div className="card-header py-3">
                                        <h6 className="m-0 font-weight-bold text-primary">DataTables Example</h6>
                                    </div>
                                    <div className="card-body">
                                        <div className="table-responsive">
                                            <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>#ID</th>
                                                        <th>Organization</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Total credits</th>
                                                        <th>Remaining Credits</th>
                                                        <th>Certification Status</th>
                                                        <th>Status</th>
                                                        <th>Date Enrolled</th>
                                                        <th>Last Booking Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th>#ID</th>
                                                        <th>Organization</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Total credits</th>
                                                        <th>Remaining Credits</th>
                                                        <th>Status</th>
                                                        <th>Date Enrolled</th>
                                                        <th>Last Booking Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    {renderTable()}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <br />


            </div >

    );
}

export default Content;